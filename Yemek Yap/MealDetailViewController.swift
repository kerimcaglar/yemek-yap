//
//  MealDetailViewController.swift
//  Yemek Yap
//
//  Created by kerimcaglar on 11.10.2018.
//  Copyright © 2018 kerimcaglar. All rights reserved.
//

import UIKit

class MealDetailViewController: UIViewController {

    @IBOutlet weak var cookImage: UIImageView!
    @IBOutlet weak var cookContent: UITextView!
    @IBOutlet weak var cookDescription: UITextView!
    
    var cookImg:UIImage?
    var cookCont:String?
    var cookDesc:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        cookImage.image = cookImg
        cookContent.text = cookCont
        cookDescription.text = cookDesc
        
        cookImage.layer.cornerRadius = 15
        cookImage.layer.masksToBounds = true
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
