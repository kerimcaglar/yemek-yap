//
//  ViewController.swift
//  Yemek Yap
//
//  Created by kerimcaglar on 10.10.2018.
//  Copyright © 2018 kerimcaglar. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var meal = Meal()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setup()
        getData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meal.title.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealCell", for: indexPath) as? MealTableViewCell
        let imageUrl = URL(string: meal.imageUrl[indexPath.row])
        let data = try? Data(contentsOf: imageUrl!)
        let image = UIImage(data: data!)
        cell?.mealImage.image = image
        cell?.mealName.text = meal.title[indexPath.row]
        cell?.person.text = meal.person[indexPath.row]
        cell?.duration.text = meal.time[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("clicked \(indexPath.row)")
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "DetailVC") as? MealDetailViewController
        let imageUrl = URL(string: meal.imageUrl[indexPath.row])
        let data = try? Data(contentsOf: imageUrl!)
        let image = UIImage(data: data!)
        detailVC?.cookImg = image
        detailVC?.cookCont = meal.ingredients[indexPath.row]
        detailVC?.cookDesc = meal.description[indexPath.row]
        self.present(detailVC!, animated: true, completion: nil)
    }
    
    func setup(){
        self.navigationController?.navigationBar.topItem?.title = "Yemekler"
        navigationController?.navigationBar.barTintColor = UIColor(red: 139/255, green: 0/255, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let lefButton = UIButton(type: .system)
        lefButton.setBackgroundImage(UIImage(named: "icon_menu"), for: .normal)
        let menuButton = UIBarButtonItem(customView: lefButton)
        menuButton.customView?.widthAnchor.constraint(equalToConstant: 30).isActive = true
        menuButton.customView?.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.navigationItem.leftBarButtonItem = menuButton
    }
    
    func getData(){
       /* Alamofire.request("http://kerimcaglar.com/yemek-tarifi").responseJSON { (response) in
            
            switch response.result{
                
            case .success(let value):
                let swiftyJsonVar = JSON(value)
                self.meal.title = []
                self.meal.person = []
                self.meal.time = []
                
                for i in 0..<swiftyJsonVar.count{
                    self.meal.title.append(String(describing: swiftyJsonVar[i]["yemek_ismi"]))
                    self.meal.person.append(String(describing: swiftyJsonVar[i]["kisi_sayisi"]))
                    self.meal.time.append(String(describing: swiftyJsonVar[i]["sure"]))
                    self.meal.imageUrl.append(String(describing: swiftyJsonVar[i]["yemek_resim"]))
                    self.meal.ingredients.append(String(describing: swiftyJsonVar[i]["malzemeler"]))
                    self.meal.description.append(String(describing: swiftyJsonVar[i]["tarif"]))
                }
            
                self.activityIndicator.stopAnimating()
                self.tableView.reloadData()
                
            case .failure(_):
                print("HATA")
            }
        }*/
        
        let myUrl = "http://kerimcaglar.com/yemek-tarifi"
        let url = URL(string: myUrl)
        let myData = try! Data(contentsOf: url!)
        let jsonDecoder = JSONDecoder()
        
        let myMeal = try? jsonDecoder.decode([MealModel].self, from: myData)
        
        for m in myMeal!{
            meal.title.append(m.yemek_ismi)
            meal.person.append(m.kisi_sayisi)
            meal.time.append(m.sure)
            meal.imageUrl.append(m.yemek_resim)
            meal.ingredients.append(m.malzemeler)
            meal.description.append(m.tarif)
        }
        
        self.activityIndicator.stopAnimating()
    }
}

