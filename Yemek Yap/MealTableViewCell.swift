//
//  MealTableViewCell.swift
//  Yemek Yap
//
//  Created by kerimcaglar on 10.10.2018.
//  Copyright © 2018 kerimcaglar. All rights reserved.
//

import UIKit

class MealTableViewCell: UITableViewCell {

    @IBOutlet weak var mealImage: UIImageView!
    @IBOutlet weak var mealName: UILabel!
    @IBOutlet weak var person: UILabel!
    @IBOutlet weak var duration: UILabel!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mealImage.layer.cornerRadius = 15
        mealImage.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
