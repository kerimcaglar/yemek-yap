//
//  Meal.swift
//  Yemek Yap
//
//  Created by kerimcaglar on 10.10.2018.
//  Copyright © 2018 kerimcaglar. All rights reserved.
//

import UIKit

struct Meal {
    var title:[String] = []
    var imageUrl:[String] = []
    var time:[String] = []
    var person:[String] = []
    var ingredients:[String] = []
    var description:[String] = []
}

struct MealModel : Codable{
    var yemek_ismi:String
    var sure:String
    var kisi_sayisi:String
    var malzemeler:String
    var tarif:String
    var yemek_resim:String
}
