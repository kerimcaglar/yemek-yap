# Yemek Yap Uygulaması - Kastamonu Üniversitesi 

## Tanım

Uygulama swift dilinin temellerinin anlatılıp, daha sonrasında iOS programlamanın uygulandığı basit bir yemek tarifi uygulamasıdır. Uygulama içinde bir web servisten verilerin çekilip, kullanıcıya gösterilmesi ve verilerin detayına gidilebilmesi eklenmiştir. Kullanıcıların uygulamayı geliştirebilmesi mümkündür. Yemek ekleme, silme gibi işlemler yapılabilir.


### Dikkat edilmesi gerekenler
Projeyi indirdikten sonra yemek-yap.xcworkspace dosyası üzerinden çalışılmalıdır. Projenize yeni bir 3.part kütüphane eklemeniz durumunda Podfile dosyasına ilgili komut eklenmeli daha sonra aşağıdaki komut çalıştırılmalıdır.

    pod install

Sorularınız için : info@kerimcaglar.com
